# Curriculum Vitae

This is my current Curriculum Vitae.

I update it nearly once in three months and can be viewed live via the GitLab page associated with it [here](https://karanjayachandra.gitlab.io/cv). The repository contains:

- A LaTeX template crated by Trey Hunner and modified by [LaTeX Templates](www.LaTeXTemplates.com).
- The content of the Curriculum Vitae itself
- git files for configuration of the repository
- GitLab CI scripts to automatically compile the LaTeX documents into a PDF

## Reuse this repository

You can reuse this repository along with the automation scripts to keep your CV updated by forking this repository. You will need to know how to write documents in LaTeX though.
